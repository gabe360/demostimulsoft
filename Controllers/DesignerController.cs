﻿using System.Collections.Generic;
using DemoStimulsoft.Data;
using DemoStimulsoft.Models;
using Microsoft.AspNetCore.Mvc;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;

namespace DemoStimulsoft.Controllers
{
    public class DesignerController : Controller
    {
        private int _reportId;
        private StiReport _report;
        private readonly IReportRepository _reportRepository;

        public DesignerController(IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }
        public IActionResult Index(string id)
        { 
            return View();
        }
      
        public IActionResult GetReport(string id)
        {
            int.TryParse(id, out _reportId);

            if (_reportId == 0)
                _reportId = 1;

            _report = _reportRepository.LoadReport(_reportId);

            return StiNetCoreDesigner.GetReportResult(this, _report);
        }

        public IActionResult PreviewReport()
        {
            StiReport report = StiNetCoreDesigner.GetActionReportObject(this);

            return StiNetCoreDesigner.PreviewReportResult(this, report);
        }

        public IActionResult SaveReport()
        {
            StiReport report = StiNetCoreDesigner.GetReportObject(this);

            // Save the report template, for example to JSON string
            report.SaveDocument(@"Reports\products.mrt");

            return StiNetCoreDesigner.SaveReportResult(this);
        }

        public IActionResult SaveReportAs()
        {
            return StiNetCoreDesigner.SaveReportResult(this);
        }

        public IActionResult DesignerEvent()
        {
            return StiNetCoreDesigner.DesignerEventResult(this);
        }
    }
}