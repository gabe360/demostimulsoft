﻿using System.Collections.Generic;
using System.Linq;
using DemoStimulsoft.Data;
using DemoStimulsoft.Models;
using Microsoft.AspNetCore.Mvc;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;

namespace DemoStimulsoft.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductRepository _productRepository;

        public HomeController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public IActionResult Index()
        {
            return View(_productRepository.GetProductCategories().ToAsyncEnumerable());
        }

        [Route("{id}")]
        public IActionResult Index(string id)
        {
            int productId;

            if (int.TryParse(id, out productId))
            {
                return View(_productRepository.Find(productId));
            }

            return View();
            
        }
    }
}