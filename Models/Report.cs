﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoStimulsoft.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string File { get; set; }
        public string DataSourceName { get; set; }
    }
}
