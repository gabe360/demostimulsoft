﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoStimulsoft.Models
{
    public class ProductCategory
    {
        public Category Category { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}
