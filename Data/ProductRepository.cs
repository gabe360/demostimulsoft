﻿using DemoStimulsoft.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DemoStimulsoft.Data
{
    public class ProductRepository : IProductRepository
    {
        public ProductRepository() { }


        public IEnumerable<Product> GetAll()
        {
            //Seed Products
            try
            {
                //Chose Encoding.GetEncoding("iso-8859-1") to handle special characters in the JSON data
                using (StreamReader sr = new StreamReader(@"data\products.json", Encoding.GetEncoding("iso-8859-1")))
                {
                    string productData = sr.ReadToEnd();
                    return JsonConvert.DeserializeObject<IEnumerable<Product>>(productData).OrderBy(p => p.Name);
                }
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public IEnumerable<Product> Find(int productId)
        {
            try
            {
                return GetAll().ToList().Where(x => x.ProductId == productId);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Category> GetCateories()
        {
            //Seed Categories
            try
            {
                //Chose Encoding.GetEncoding("iso-8859-1") to handle special characters in the JSON data
                using (StreamReader sr = new StreamReader(@"data\categories.json", Encoding.GetEncoding("iso-8859-1")))
                {
                    string categoryData = sr.ReadToEnd();
                    return JsonConvert.DeserializeObject<IEnumerable<Category>>(categoryData).OrderBy(c=>c.CategoryId);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IEnumerable<ProductCategory> GetProductCategories()
        {
            List<Product> products = GetAll().ToList<Product>();
            List<ProductCategory> pCategories = new List<ProductCategory>();
            foreach(Category category in GetCateories())
            {
                pCategories.Add(new ProductCategory
                {
                    Category = category,
                    Products = products.Where(p => p.CategoryId == category.CategoryId)
                });
            }

            return pCategories;
        }

    }
}
