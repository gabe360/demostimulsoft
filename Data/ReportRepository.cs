﻿using DemoStimulsoft.Models;
using Newtonsoft.Json;
using Stimulsoft.Report;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DemoStimulsoft.Data
{
    public class ReportRepository :IReportRepository
    {
        private readonly IProductRepository _productRepository;

        public ReportRepository(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public IEnumerable<Report> GetAll()
        {
            //Seed Products
            try
            {
                //Chose Encoding.GetEncoding("iso-8859-1") to handle special characters in the JSON data
                using (StreamReader sr = new StreamReader(@"data\reports.json", Encoding.GetEncoding("iso-8859-1")))
                {
                    string reportData = sr.ReadToEnd();
                    return JsonConvert.DeserializeObject<IEnumerable<Report>>(reportData).OrderBy(p => p.Name);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Report FindById(int reportId)
        {
            return GetAll().Where(x => x.Id == reportId).FirstOrDefault();
        }

        public IEnumerable<Report> FindByName(string reportName)
        {
            return GetAll().Where(x => x.Name.ToLower().Contains(reportName.ToLower()));
        }

        public StiReport LoadReport(int reportId)
        {
            StiReport report = new StiReport();
            Report reportConfig = FindById(reportId);
            if (reportConfig != null)
            {
                if (System.IO.File.Exists(string.Format(@"Reports\{0}", reportConfig.File)))
                {
                    report.Load(string.Format(@"Reports\{0}", reportConfig.File));   
                }

                int businessObjectLevel = 1;
                switch (reportConfig.Id)
                {
                    case (1):
                        IEnumerable<Product> products = _productRepository.GetAll();
                        report.RegBusinessObject(reportConfig.DataSourceName, products);
                        break;
                    case (2):
                        //IEnumerable<ProductCategory> productCategories = _productRepository.GetProductCategories();
                        businessObjectLevel = 2;
                        IEnumerable<ProductCategory> categories = _productRepository.GetProductCategories();
                        report.RegBusinessObject(reportConfig.DataSourceName, categories);
                        break;
                    default:
                        break;
                }

                report.ReportName = reportConfig.Name;
                report.Dictionary.SynchronizeBusinessObjects(businessObjectLevel);
            }

            return report;
        }

    }
}
