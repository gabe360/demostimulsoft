﻿using DemoStimulsoft.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DemoStimulsoft.Data
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
        IEnumerable<Product> Find(int productId);
        IEnumerable<Category> GetCateories();
        IEnumerable<ProductCategory> GetProductCategories();
    }
}