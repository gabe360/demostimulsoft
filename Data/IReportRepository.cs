﻿using DemoStimulsoft.Models;
using Stimulsoft.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoStimulsoft.Data
{
    public interface IReportRepository
    {
        IEnumerable<Report> GetAll();
        Report FindById(int reportId);
        IEnumerable<Report> FindByName(string reportName);
        StiReport LoadReport(int reportId);
    }
}
